﻿using UnityEngine;

namespace PathGeneration.Enviroments
{
    /// <summary>
    /// ScriptableObject класс, хранящий данные о настройке дороги.
    /// </summary>
    [CreateAssetMenu(fileName = "PathSettings", menuName = "SO/PathSettings")]
    public class PathSettings : ScriptableObject
    {
        /// <summary>
        /// Свойство материала дороги
        /// </summary>
        [field: SerializeField]
        public Material Material { get; set; }

        /// <summary>
        /// Свойство материала радиуса искревления дороги
        /// </summary>
        [field: SerializeField]
        public float CurveRadius { get; set; } = 1;

        /// <summary>
        /// Свойство ширины дороги
        /// </summary>
        [field: SerializeField]
        public float Width { get; set; } = 1;

        /// <summary>
        /// Свойство количеста сегментов в повороте
        /// </summary>
        [field: SerializeField]
        public int CurveSectorCount { get; set; } = 10;
    }
}
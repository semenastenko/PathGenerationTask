﻿using UnityEngine;

namespace PathGeneration.Enviroments
{
    /// <summary>
    /// Класс утилита для raycast
    /// </summary>
    public class RaycastUtility
    {
        /// <summary>
        /// Поле камеры
        /// </summary>
        private readonly Camera _camera;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="camera">камера</param>
        public RaycastUtility(Camera camera)
        {
            _camera = camera;
        }

        /// <summary>
        /// Метод нахождения соприкосновения точки с объектом с экрана в мировом пространстве
        /// </summary>
        /// <param name="mousePosition">Позиция мыши</param>
        /// <returns>Данные о соприкосновении с объектом</returns>
        public RaycastHit? ScreenPointToHit(Vector2 mousePosition)
        {
            var ray = _camera.ScreenPointToRay(mousePosition);
            if (Physics.Raycast(ray, out var hit))
            {
                return hit;
            }
            return null;
        }
    }
}
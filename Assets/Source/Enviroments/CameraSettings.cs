﻿using UnityEngine;

namespace PathGeneration.Enviroments
{
    /// <summary>
    /// ScriptableObject класс, хранящий данные о настройке камеры.
    /// </summary>
    [CreateAssetMenu(fileName = "CameraSettigns", menuName = "SO/CameraSettings")]
    public class CameraSettings : ScriptableObject
    {
        /// <summary>
        /// Свойство скорости камеры
        /// </summary>
        [field: SerializeField]
        public float CameraSpeed { get; set; } = 10;

        /// <summary>
        /// Свойство ускоренной скорости камеры
        /// </summary>
        [field: SerializeField]
        public float CameraShiftSpeed { get; set; } = 50;

        /// <summary>
        /// Свойство чувствительности мыши
        /// </summary>
        [field: SerializeField]
        public float MouseSensetivity { get; set; } = 100;

        /// <summary>
        /// Свойство множетеля зума
        /// </summary>
        [field: SerializeField]
        public float ZoomMultiple { get; set; } = 5;

        /// <summary>
        /// Свойство высоты верхнего просмотра
        /// </summary>
        [field: SerializeField]
        public float TopViewHeight { get; set; } = 50;

        /// <summary>
        /// Свойство максимального поворота Y камеры
        /// </summary>
        [field: SerializeField]
        public float MaxCameraYAngle { get; set; } = 180;
    }
}
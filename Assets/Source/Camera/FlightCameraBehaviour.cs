﻿using UnityEngine;
using UnityEngine.InputSystem;
using PathGeneration.Input;
using Cinemachine;
using PathGeneration.Enviroments;

namespace PathGeneration.Cameras
{
    /// <summary>
    ///Класс поведения свободного перемещения камеры от 1 лица
    /// </summary>
    public class FlightCameraBehaviour : BaseCameraBehaviour, CameraInput.IFlightActions
    {
        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="brain">CinemachineBrain</param>
        /// <param name="target">цель камеры</param>
        /// <param name="settings">настройки камеры</param>
        /// <param name="input">управление поведением</param>
        public FlightCameraBehaviour(CinemachineBrain brain, CameraTarget target, CameraSettings settings, CameraInput input)
            : base(brain, target, settings, new InputActivationHandler(input.Flight.Enable, input.Flight.Disable))
        {
            input.Flight.SetCallbacks(this);
        }


        /// <summary>
        /// Метод обработки включения поведения
        /// </summary>
        protected override void OnEnable()
        {
            _target.Speed = _settings.CameraSpeed;
        }

        /// <summary>
        /// Метод обработки выключения поведения
        /// </summary>
        protected override void OnDisable()
        {
            _target.MoveDirection = Vector3.zero;
            _target.RotationDirection = Vector3.zero;
        }

        /// <summary>
        /// Метод свободного осмотра от 1 лица
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnLook(InputAction.CallbackContext context)
        {
            var axis = context.ReadValue<Vector2>();
            var newAngle = new Vector2(-axis.y, axis.x);
            var targetAngle = _target.EulerAngles;
            targetAngle.x = targetAngle.x > 180 ? targetAngle.x - 360 : targetAngle.x;
            targetAngle.y = targetAngle.y > 180 ? targetAngle.y - 360 : targetAngle.y;
            newAngle.x = Mathf.Clamp(targetAngle.x + newAngle.x, -_settings.MaxCameraYAngle, _settings.MaxCameraYAngle) - targetAngle.x;
            _target.RotationDirection = newAngle * _settings.MouseSensetivity;
        }

        /// <summary>
        /// Метод инициализации виртуальной камеры
        /// </summary>
        protected override void InitializeVirtualCamera()
        {
            _virtualCamera.Follow = TargetTransform;
            _virtualCamera.AddCinemachineComponent<CinemachineHardLockToTarget>();
            _virtualCamera.AddCinemachineComponent<CinemachineSameAsFollowTarget>();
            _virtualCamera.m_Lens.ModeOverride = LensSettings.OverrideModes.Perspective;
            _virtualCamera.m_Priority = 10;
            _virtualCamera.name = "VC_Flight";
        }

        /// <summary>
        /// Метод фиксирования курсура
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnLookAround(InputAction.CallbackContext context)
        {
            switch (context.phase)
            {
                case InputActionPhase.Started:
                    Cursor.lockState = CursorLockMode.Locked;
                    break;
                case InputActionPhase.Canceled:
                    Cursor.lockState = CursorLockMode.None;
                    break;
            }
        }

        /// <summary>
        /// Метод ускорения передвижения
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnSpeedUp(InputAction.CallbackContext context)
        {
            switch (context.phase)
            {
                case InputActionPhase.Started:
                    _target.Speed = _settings.CameraShiftSpeed;
                    break;
                case InputActionPhase.Canceled:
                    _target.Speed = _settings.CameraSpeed;
                    break;
            }
        }

        /// <summary>
        /// Метод перемещения от 1 лица
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnMove(InputAction.CallbackContext context)
        {
            if (context.performed || context.canceled)
            {
                _target.MoveDirection = context.ReadValue<Vector3>();
            }
        }
    }
}

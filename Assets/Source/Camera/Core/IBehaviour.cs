﻿using System.Threading.Tasks;

namespace PathGeneration.Cameras
{
    /// <summary>
    /// Интерфейс поведения
    /// </summary>
    public interface IBehaviour
    {
        /// <summary>
        /// Включение поведения
        /// </summary>
        void Enable();

        /// <summary>
        /// Выключение поведения
        /// </summary>
        void Disable();
    }
}

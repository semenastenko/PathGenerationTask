﻿namespace PathGeneration.Cameras
{
    /// <summary>
    /// Интерфейс держателя поведения по умолчанию
    /// </summary>
    public interface IDefaultBehaviourHolder
    {
        /// <summary>
        /// Свойстов поведения
        /// </summary>
        public IBehaviour Behaviour { get; }
    }
}

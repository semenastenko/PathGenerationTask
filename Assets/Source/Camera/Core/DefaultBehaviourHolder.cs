﻿namespace PathGeneration.Cameras
{
    /// <summary>
    /// Обобщеный класс держателя поведения по умолчанию
    /// </summary>
    /// <typeparam name="T">Обобщеный тип поведения</typeparam>
    public class DefaultBehaviourHolder<T> : IDefaultBehaviourHolder
        where T : IBehaviour
    {
        /// <summary>
        /// Поле поведения
        /// </summary>
        private readonly IBehaviour _behaviour;

        /// <summary>
        /// Свойстов поведения
        /// </summary>
        public IBehaviour Behaviour => _behaviour;

        /// <summary>
        /// Конструстор класса
        /// </summary>
        /// <param name="behaviour">поведения для хранения в держателе</param>
        public DefaultBehaviourHolder(T behaviour)
        {
            _behaviour = behaviour;
        }
    }
}

﻿using System;

namespace PathGeneration.Cameras
{
    /// <summary>
    /// Класс обработчик активации управления
    /// </summary>
    public class InputActivationHandler
    {
        /// <summary>
        /// Поле делегата включения
        /// </summary>
        private readonly Action _onEnable;

        /// <summary>
        /// Поле делегата выключения
        /// </summary>
        private readonly Action _onDisable;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="onEnable">делегат включения</param>
        /// <param name="onDisable">делегат выключения</param>
        public InputActivationHandler(Action onEnable, Action onDisable)
        {
            _onEnable = onEnable;
            _onDisable = onDisable;
        }

        /// <summary>
        /// Метод вызова делегата включения
        /// </summary>
        public void Enable() => _onEnable?.Invoke();

        /// <summary>
        /// Метод вызова делегата выключения
        /// </summary>
        public void Disable() => _onDisable?.Invoke();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PathGeneration.Cameras
{
    /// <summary>
    /// ����� MonoBehaviour ������� ���� ������
    /// </summary>
    public class CameraTarget : MonoBehaviour
    {
        /// <summary>
        /// �������� ������� ����
        /// </summary>
        public Vector3 Position
        {
            get => transform.position;
            set => transform.position = value;
        }

        /// <summary>
        /// �������� �������� ����
        /// </summary>
        public Quaternion Rotation
        {
            get => transform.rotation;
            set => transform.rotation = value;
        }

        /// <summary>
        /// �������� ���� ������ ����
        /// </summary>
        public Vector3 EulerAngles => transform.eulerAngles;

        /// <summary>
        /// �������� �������� ����
        /// </summary>
        public float Speed { get; set; }

        /// <summary>
        /// �������� ����������� �������� ����
        /// </summary>
        public Vector3 MoveDirection { get; set; }

        /// <summary>
        /// �������� ����������� �������� ����
        /// </summary>
        public Vector3 RotationDirection { get; set; }


        /// <summary>
        /// �����, ������������ ������ ����
        /// </summary>
        private void Update()
        {
            if (MoveDirection.magnitude != 0)
            {
                transform.Translate(MoveDirection * Speed * Time.deltaTime);
            }
            if (RotationDirection.magnitude != 0)
            {
                transform.rotation = Quaternion.Euler(
                    EulerAngles.x + RotationDirection.x * Time.deltaTime, 
                    EulerAngles.y + RotationDirection.y * Time.deltaTime, 0);
            }
        }
    }
}

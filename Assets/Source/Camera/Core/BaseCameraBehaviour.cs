﻿using Cinemachine;
using PathGeneration.Enviroments;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

namespace PathGeneration.Cameras
{
    /// <summary>
    ///Базовый класс поведения камеры
    /// </summary>
    public abstract class BaseCameraBehaviour : IBehaviour
    {
        /// <summary>
        /// Поле цели для камеры
        /// </summary>
        protected readonly CameraTarget _target;

        /// <summary>
        /// Поле Cinemachin Brain
        /// </summary>
        protected readonly CinemachineBrain _brain;

        /// <summary>
        /// Поле насроек камеры
        /// </summary>
        protected readonly CameraSettings _settings;

        /// <summary>
        /// Поле виртуальной камеры
        /// </summary>
        protected readonly CinemachineVirtualCamera _virtualCamera;

        /// <summary>
        /// Поле обратотчика для активации управления
        /// </summary>
        protected readonly InputActivationHandler _inputHandler;

        /// <summary>
        /// Поле отслеживания завершения перехода камеры из состояний
        /// </summary>
        private TaskCompletionSource<bool> transitionCompletition;

        /// <summary>
        /// Свойстов Transform цели камеры
        /// </summary>
        public Transform TargetTransform => _target.transform;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="brain">CinemachineBrain</param>
        /// <param name="target">цель камеры</param>
        /// <param name="settings">настройки камеры</param>
        /// <param name="inputHandler">обратотчик для активации управления</param>
        public BaseCameraBehaviour(CinemachineBrain brain, CameraTarget target, CameraSettings settings, InputActivationHandler inputHandler)
        {
            _brain = brain;
            _target = target;
            _settings = settings;
            _inputHandler = inputHandler;
            _inputHandler.Disable();
            _virtualCamera = new GameObject().AddComponent<CinemachineVirtualCamera>();
            _virtualCamera.enabled = false;
            InitializeVirtualCamera();
            _brain.m_CameraActivatedEvent.AddListener(OnTransition);
        }

        /// <summary>
        /// Метод инициализации виртуальной камеры
        /// </summary>
        protected abstract void InitializeVirtualCamera();

        /// <summary>
        /// Метод обработки включения поведения
        /// </summary>
        protected virtual void OnEnable() { }

        /// <summary>
        /// Метод обработки выключения поведения
        /// </summary>
        protected virtual void OnDisable() { }

        /// <summary>
        /// Метод включения активации
        /// </summary>
        public async void Enable()
        {
            _virtualCamera.enabled = true;
            transitionCompletition = new TaskCompletionSource<bool>();
            OnEnable();
            await transitionCompletition.Task;
            _inputHandler.Enable();
        }

        /// <summary>
        /// Метод выключения активации
        /// </summary>
        public void Disable()
        {
            _inputHandler.Disable();
            _virtualCamera.enabled = false;
            OnDisable();
        }

        /// <summary>
        /// Метод обратотки события перехода камеры из состояний
        /// </summary>
        /// <param name="oldCamera">От какой камеры</param>
        /// <param name="newCamera">К какой камере</param>
        private async void OnTransition(ICinemachineCamera oldCamera, ICinemachineCamera newCamera)
        {
            while (_brain != null && _brain.IsBlending)
            {
                await Task.Yield();
            }
            transitionCompletition?.TrySetResult(true);
        }
    }
}

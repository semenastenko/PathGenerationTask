﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace PathGeneration.Cameras
{
    /// <summary>
    /// Класс менеджер помедений камеры
    /// </summary>
    public class CameraBehaviourManager
    {
        /// <summary>
        /// поле списка всех поведений
        /// </summary>
        private List<IBehaviour> _behaviours;

        /// <summary>
        /// поле текущего поведения
        /// </summary>
        private IBehaviour _current;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="behaviours">список поведений</param>
        /// <param name="holder">держатель поведения по умолчанию</param>
        public CameraBehaviourManager(List<IBehaviour> behaviours, IDefaultBehaviourHolder holder)
        {
            _behaviours = behaviours;

            SetBehaviour(holder.Behaviour);
        }

        /// <summary>
        /// Обобщенный метод установки нового поведения 
        /// </summary>
        /// <typeparam name="T">Обобщеный тип поведения камеры</typeparam>
        public void SetBehaviour<T>() where T : IBehaviour
        {
            var behaviour = _behaviours.Where(x => x is T).FirstOrDefault();
            if (behaviour != null)
                SetBehaviour(behaviour);
            else
                Debug.LogWarning($"Behaviour {typeof(T)} dosn't contains in manager!");

        }

        /// <summary>
        /// Метод установки нового поведения 
        /// </summary>
        /// <param name="state">новое поведение</param>
        protected void SetBehaviour(IBehaviour state)
        {
            if (_current == state) return;
            _current?.Disable();
            _current = state;
            _current?.Enable();
        }
    }
}

﻿using UnityEngine;
using UnityEngine.InputSystem;
using PathGeneration.Input;
using Cinemachine;
using PathGeneration.Enviroments;

namespace PathGeneration.Cameras
{
    /// <summary>
    ///Класс поведения перемещения камеры с верхнего вида 
    /// </summary>
    public class TopViewCameraBehaviour : BaseCameraBehaviour, CameraInput.ITopViewActions
    {
        /// <summary>
        /// Поле начала перетаскивания
        /// </summary>
        private Vector3 _dragOrigin;

        /// <summary>
        /// Поле последней позиции мыши
        /// </summary>
        private Vector2 _lastMousePosition;

        /// <summary>
        /// Поле класса для фиксирования Y-позиции камеры
        /// </summary>
        private CustomLockCameraY _lockCameraY;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="brain">CinemachineBrain</param>
        /// <param name="target">цель камеры</param>
        /// <param name="settings">настройки камеры</param>
        /// <param name="input">управление поведением</param>
        public TopViewCameraBehaviour(CinemachineBrain brain, CameraTarget target, CameraSettings settings, CameraInput input)
            : base(brain, target, settings, new InputActivationHandler(input.TopView.Enable, input.TopView.Disable))
        {
            input.TopView.SetCallbacks(this);
        }

        /// <summary>
        /// Метод обработки включения поведения
        /// </summary>
        protected override void OnEnable()
        {
            _target.Speed = _settings.CameraShiftSpeed;
            _lockCameraY.m_YPosition = _settings.TopViewHeight;

            SetVirtualCameraRotation(_target.EulerAngles.y);
        }

        /// <summary>
        /// Метод обработки выключения поведения
        /// </summary>
        protected override void OnDisable()
        {
            _target.MoveDirection = Vector3.zero;
            _target.RotationDirection = Vector3.zero;
        }

        /// <summary>
        /// Метод инициализации виртуальной камеры
        /// </summary>
        protected override void InitializeVirtualCamera()
        {
            _virtualCamera.transform.eulerAngles = Vector3.right * 90;
            _virtualCamera.Follow = TargetTransform;
            _virtualCamera.AddCinemachineComponent<CinemachineHardLockToTarget>();
            _lockCameraY = _virtualCamera.gameObject.AddComponent<CustomLockCameraY>();
            _virtualCamera.AddExtension(_lockCameraY);
            _virtualCamera.m_Lens.ModeOverride = LensSettings.OverrideModes.Perspective;
            _virtualCamera.m_Priority = 11;
            _virtualCamera.name = "VC_TopView";

        }

        /// <summary>
        /// Метод перемещения камеры с верхнего вида, используя клавиатуру
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnMove(InputAction.CallbackContext context)
        {
            if (context.performed || context.canceled)
            {
                var direction = context.ReadValue<Vector2>();

                _target.MoveDirection = new Vector3(direction.x, direction.y, 0);
            }
        }

        /// <summary>
        /// Метод зума камеры с верхнего вида
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnZoom(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                var delta = context.ReadValue<float>();
                delta = Mathf.Min(1, Mathf.Abs(delta)) * Mathf.Sign(delta);
                var size = Mathf.Max(1, _lockCameraY.m_YPosition + delta * _settings.ZoomMultiple);
                _lockCameraY.m_YPosition = size;
            }
        }

        /// <summary>
        /// Метод перемещения камеры с верхнего вида, используя мышь
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnMouseMove(InputAction.CallbackContext context)
        {
            Vector3 mousePosition = context.ReadValue<Vector2>();
            mousePosition.z = _lockCameraY.m_YPosition;
            var point = _brain.OutputCamera.ScreenToWorldPoint(mousePosition);
            switch (context.phase)
            {
                case InputActionPhase.Started:
                    _dragOrigin = point;
                    break;
                case InputActionPhase.Performed:
                    var direction = _dragOrigin - point;
                    _target.Position += direction;
                    break;
            }
        }

        /// <summary>
        /// Метод вращения камеры с верхнего вида, используя мышь
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnMouseRotate(InputAction.CallbackContext context)
        {
            var mousePosition = context.ReadValue<Vector2>();
            
            switch (context.phase)
            {
                case InputActionPhase.Started:
                    _lastMousePosition = mousePosition;
                    break;
                case InputActionPhase.Performed:
                    var center = new Vector2(Screen.width / 2, Screen.height / 2);
                    var startDirection = _lastMousePosition - center;
                    var endDirection = mousePosition - center;
                    var angle = Vector2.SignedAngle(startDirection, endDirection);
                    var rotation = _virtualCamera.transform.eulerAngles.y + angle;
                    SetVirtualCameraRotation(rotation);
                    SetTargetRotation(rotation);
                    _lastMousePosition = mousePosition;
                    break;
            }
        }

        /// <summary>
        /// Метод установки вращения виртуальной камеры
        /// </summary>
        /// <param name="angle">угол вращения камеры</param>
        private void SetVirtualCameraRotation(float angle)
        {
            var rotation = _virtualCamera.transform.eulerAngles;
            rotation.y = angle;
            _virtualCamera.transform.eulerAngles = rotation;
        }

        /// <summary>
        /// Метод установки вращения цели
        /// </summary>
        /// <param name="angle">угол вращения цели</param>
        private void SetTargetRotation(float angle)
        {
            var rotation = _target.EulerAngles;
            rotation.y = angle;
            _target.Rotation = Quaternion.Euler(rotation);
        }
    }
}

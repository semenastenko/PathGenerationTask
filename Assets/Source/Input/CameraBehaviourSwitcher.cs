﻿using PathGeneration.Cameras;
using UnityEngine.InputSystem;

namespace PathGeneration.Input
{
    /// <summary>
    /// Класс переключатель поведения камеры
    /// </summary>
    public class CameraBehaviourSwitcher : CameraStateInput.IBaseActions
    {
        /// <summary>
        /// Поле менеджера поведений камеры
        /// </summary>
        private CameraBehaviourManager _behaviourManager;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="behaviourManager">менеджер поведений камеры</param>
        /// <param name="input">управление камерой</param>
        public CameraBehaviourSwitcher(CameraBehaviourManager behaviourManager, CameraStateInput input)
        {
            _behaviourManager = behaviourManager;
            input.Base.SetCallbacks(this);
            input.Enable();
        }

        /// <summary>
        /// Метод включение поведения свободного перемещения камеры от 1 лица
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnFlightBehavior(InputAction.CallbackContext context)
        {
            _behaviourManager.SetBehaviour<FlightCameraBehaviour>();
        }

        /// <summary>
        /// Метод включение поведения перемещения камеры с верхнего вида 
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnTopViewBehaviour(InputAction.CallbackContext context)
        {
            _behaviourManager.SetBehaviour<TopViewCameraBehaviour>();
        }
    }
}

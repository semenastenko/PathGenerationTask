﻿using PathGeneration.Core;
using UnityEngine;

namespace PathGeneration.Interactions
{
    /// <summary>
    /// Класс MonoBehaviour объекта указателя точки поворота дороги
    /// </summary>
    public class PathPointer : MonoBehaviour, IInteractable
    {
        /// <summary>
        /// Поле коллайдера объекта
        /// </summary>
        private Collider _collider;

        /// <summary>
        /// Свойство позиции объекта
        /// </summary>
        public Vector3 Position { get => transform.position; private set => transform.position = value; }

        /// <summary>
        /// Свойство точки поворота дороги
        /// </summary>
        public PathPoint Point { get; private set; }

        /// <summary>
        /// Свойство выбран ли объект
        /// </summary>
        public bool IsSelected { get; private set; }

        /// <summary>
        /// Метод инициализации MonoBehaviour
        /// </summary>
        private void Awake()
        {
            _collider = gameObject.GetComponent<Collider>();
        }

        /// <summary>
        /// Метод выбора объета
        /// </summary>
        /// <param name="value">Если true - объект выбран, если false - не выбран</param>
        public void SetSelected(bool value)
        {
            IsSelected = value;
            _collider.enabled = !value;
        }

        /// <summary>
        /// Метод назначения точки поворота дороги
        /// </summary>
        /// <param name="path">точка поворота дороги</param>
        public void SetPoint(PathPoint path)
        {
            if (Point != null)
                Point.PropertyChanged -= PointPositionChanged;

            Point = path;
            Point.PropertyChanged += PointPositionChanged;
        }


        /// <summary>
        /// Метод обработки события изменения позиции точки поворота доргои
        /// </summary>
        /// <param name="sender">отправитель</param>
        /// <param name="e">Агрументы события</param>
        private void PointPositionChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(PathPoint.Position))
            {
                Position = Point.Position + Vector3.up / 2;
            }
        }

        /// <summary>
        /// Метод отображения указателя
        /// </summary>
        public void Show()
        {
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Метод скрытия указателя
        /// </summary>
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        /// <summary>
        /// Метод уничтожения указателя
        /// </summary>
        public void Destroy()
        {
            Point.PropertyChanged -= PointPositionChanged;
            Destroy(gameObject);
        }

        /// <summary>
        /// Метод взаимодействия с указателем
        /// </summary>
        public void Interact()
        {
            IsSelected = !IsSelected;
            SetSelected(IsSelected);
        }
    }
}
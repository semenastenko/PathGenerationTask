﻿using UnityEngine;

namespace PathGeneration.Interactions
{
    /// <summary>
    /// Класс MonoBehaviour объекта поверхности
    /// </summary>
    public class Ground : MonoBehaviour, IInteractable
    {
        /// <summary>
        /// Позиция объекта
        /// </summary>
        public Vector3 Position => transform.position;

        /// <summary>
        /// Метод взаимодействия с объектом
        /// </summary>
        public void Interact()
        {
            
        }
    }
}
﻿namespace PathGeneration.Interactions
{
    /// <summary>
    /// Интерфейс взаимодействия
    /// </summary>
    public interface IInteractable 
    {
        /// <summary>
        /// Метод взаимодействия
        /// </summary>
        void Interact();
    }
}
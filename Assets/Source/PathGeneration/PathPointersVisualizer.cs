﻿using PathGeneration.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PathGeneration.Core
{
    /// <summary>
    /// Класс визуализации указателей поворотов дороги
    /// </summary>
    public class PathPointersVisualizer : IDisposable
    {
        /// <summary>
        /// Поле списка указателей
        /// </summary>
        private readonly List<PathPointer> _pointers = new();

        /// <summary>
        /// Поле целевого объекта точки
        /// </summary>
        private readonly PathComponent _target;

        /// <summary>
        /// Поле флага отображения указателей
        /// </summary>
        private bool _isVisible;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="path">целевой объекта точки</param>
        public PathPointersVisualizer(PathComponent path)
        {
            _target = path;
            _target.CollectionChangedEvent.CollectionChanged += OnCollectionChanged;
        }

        /// <summary>
        /// Метод отображения указателей
        /// </summary>
        public void Show()
        {
            _isVisible = true;
            foreach (var pointer in _pointers)
            {
                pointer.Show();
            }
        }

        /// <summary>
        /// Метод скрытия указателей
        /// </summary>
        public void Hide()
        {
            _isVisible = false;
            foreach (var pointer in _pointers)
            {
                pointer.Hide();
            }
        }

        /// <summary>
        /// Метод получения индекса указателя
        /// </summary>
        /// <param name="pointer">указателя</param>
        /// <returns>индекс указателя</returns>
        public int GetIndex(PathPointer pointer)
        {
            return _pointers.IndexOf(pointer);
        }

        /// <summary>
        /// Метод обработчик события изменения коллекции точек поворотов дороги
        /// </summary>
        /// <param name="sender">отправитель</param>
        /// <param name="e">аргументы события</param>
        private void OnCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    var newItems = e.NewItems.Cast<PathPoint>();
                    _pointers.InsertRange(e.NewStartingIndex, newItems.Select(AddPointer));
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    RemovePointer(e.OldStartingIndex);
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    RemoveAll();
                    break;
            }
        }

        /// <summary>
        /// Метод добавления нового указателя
        /// </summary>
        /// <param name="point">точка поворота дороги</param>
        /// <returns>новый указателя</returns>
        private PathPointer AddPointer(PathPoint point)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = point.Position + Vector3.up / 2;
            var pointer = sphere.AddComponent<PathPointer>();
            pointer.SetPoint(point);
            if (_isVisible)
                pointer.Show();
            else
                pointer.Hide();

            return pointer;
        }

        /// <summary>
        /// Метод удаления указателя
        /// </summary>
        /// <param name="pointer">указатель</param>
        private void RemovePointer(PathPointer pointer)
        {
            _pointers.Remove(pointer);
            pointer.Destroy();
        }

        /// <summary>
        /// Метод удаления указателя по указателю
        /// </summary>
        /// <param name="index">индекс указатель</param>
        private void RemovePointer(int index)
        {
            var pointer = _pointers[index];
            _pointers.Remove(pointer);
            pointer.Destroy();
        }

        /// <summary>
        /// Метод удаления всех указателей
        /// </summary>
        private void RemoveAll()
        {
            foreach (var item in _pointers.ToList())
            {
                RemovePointer(item);
            }
        }

        /// <summary>
        /// Метод утилизации класса
        /// </summary>
        public void Dispose()
        {
            _target.CollectionChangedEvent.CollectionChanged -= OnCollectionChanged;
            RemoveAll();
        }
    }
}

﻿using PathGeneration.Enviroments;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using UnityEngine;

namespace PathGeneration.Core
{
    /// <summary>
    /// Класс MonoBehaviour объекта дороги
    /// </summary>
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class PathComponent : MonoBehaviour
    {
        /// <summary>
        /// Поле наблюдаемой коллекции точек поворота дороги
        /// </summary>
        private readonly ObservableCollection<PathPoint> _points = new();

        /// <summary>
        /// Поле фильтра меша
        /// </summary>
        private MeshFilter _meshFilter;

        /// <summary>
        /// Поле рендерера меша
        /// </summary>
        private MeshRenderer _meshRenderer;

        /// <summary>
        /// Поле фабрики меша дороги
        /// </summary>
        private PathMeshFactory _pathFactory;

        /// <summary>
        /// Поле настроек дороги
        /// </summary>
        private PathSettings _pathSettings;

        /// <summary>
        /// Свойство обработчика события изменения коллекции точек поворота дороги
        /// </summary>
        public INotifyCollectionChanged CollectionChangedEvent => _points;

        /// <summary>
        /// Свойство списка точек поворота дороги
        /// </summary>
        public IReadOnlyList<PathPoint> Points => _points;

        /// <summary>
        /// Метод инициализации MonoBehaviour
        /// </summary>
        private void Awake()
        {
            _meshFilter = gameObject.GetComponent<MeshFilter>();
            _meshRenderer = gameObject.GetComponent<MeshRenderer>();
            _pathFactory = new PathMeshFactory();
            _points.CollectionChanged += OnPointsCollectionChanged;
        }

        /// <summary>
        /// Метод установки настроек дороги
        /// </summary>
        /// <param name="settigns">настройки дороги</param>
        public void SetPathSettings(PathSettings settigns)
        {
            _pathSettings = settigns;
        }

        /// <summary>
        /// Метод добавления точки поворота в конец дороги
        /// </summary>
        /// <param name="position">позиция точки</param>
        public void AddPoint(Vector3 position)
        {
            _points.Add(CreatePoint(position));
        }

        /// <summary>
        /// Метод добавления точки поворота дороги по индексу
        /// </summary>
        /// <param name="index">индекс вставки</param>
        /// <param name="position">позиция точки</param>
        public void InsertPoint(int index, Vector3 position)
        {
            _points.Insert(index, CreatePoint(position));
        }

        /// <summary>
        /// Метод удаления точки поворота дороги по индексу
        /// </summary>
        /// <param name="index">индекс удаления</param>
        public void RemovePoint(int index)
        {
            if (index >= 0 && index < _points.Count)
            {
                var point = _points[index];
                RemovePoint(point);
            }
        }

        /// <summary>
        /// Метод удаления всех точек поворота дороги
        /// </summary>
        public void Clear()
        {
            foreach (var point in _points)
            {
                RemovePoint(point);
            }
            _points.Clear();
        }

        /// <summary>
        /// Метод создания точки поворота дороги
        /// </summary>
        /// <param name="position">позиция точки</param>
        /// <returns>Точка поворота дороги</returns>
        private PathPoint CreatePoint(Vector3 position)
        {
            var point = new PathPoint(position);
            point.PropertyChanged += PointPositionChanged;
            return point;
        }

        /// <summary>
        /// Метод удаления точки поворота дороги
        /// </summary>
        /// <param name="point"></param>
        private void RemovePoint(PathPoint point)
        {
            point.PropertyChanged -= PointPositionChanged;
            _points.Remove(point);
        }

        /// <summary>
        /// Метод обработчик события обновления коллекции точек поворота дороги
        /// </summary>
        /// <param name="sender">отправитель</param>
        /// <param name="e">аргументы события</param>
        private void OnPointsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateMesh();
        }

        /// <summary>
        /// Метод обработчик события обновления позиции точки поворота дороги
        /// </summary>
        /// <param name="sender">отправитель</param>
        /// <param name="e">аргументы события</param>
        private void PointPositionChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(PathPoint.Position))
            {
                UpdateMesh();
            }
            else if (e.PropertyName == nameof(PathPoint.MustDestroy))
            {
                var point = sender as PathPoint;
                if (point.MustDestroy)
                {
                    RemovePoint(point);
                }
            }
        }

        /// <summary>
        /// Метод обновления меша
        /// </summary>
        private void UpdateMesh()
        {
            _meshFilter.mesh = _pathFactory.Create(
                _points.Select(x => x.Position),
                _pathSettings.Width, 
                _pathSettings.CurveRadius, 
                _pathSettings.CurveSectorCount);
        }

        /// <summary>
        /// Метод установки материала
        /// </summary>
        /// <param name="material">материал</param>
        public void SetMaterial(Material material)
        {
            _meshRenderer.sharedMaterial = material;
        }

        /// <summary>
        /// Метод уничтожения дороги
        /// </summary>
        public void Destroy()
        {
            _points.CollectionChanged -= OnPointsCollectionChanged;
            Destroy(gameObject);
        }
    }
}

using PathGeneration.Enviroments;
using PathGeneration.Interactions;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Zenject;

namespace PathGeneration.Core
{
    /// <summary>
    /// ����� ������������� �����  
    /// </summary>
    public class PathMapper : IInitializable
    {
        /// <summary>
        /// ���� �������� ������
        /// </summary>
        private readonly PathSettings _settings;

        /// <summary>
        /// ���� ������� ������� ������
        /// </summary>
        private readonly PathComponentFactory _factory;

        /// <summary>
        /// ���� ������� ������������� ���������� ��� ������� ������
        /// </summary>
        private readonly Dictionary<PathComponent, PathPointersVisualizer> _paths;

        /// <summary>
        /// ���� ��������� ��������� ������
        /// </summary>
        private PathComponent _selectedPath;

        /// <summary>
        /// ���� ���������� ����������
        /// </summary>
        private PathPointer _selectedPointer;

        /// <summary>
        /// ���� ����� ����������� ����������
        /// </summary>
        private bool _isVisiblePointers;

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="settings">��������� ������</param>
        public PathMapper(PathSettings settings)
        {
            _settings = settings;
            _factory = new PathComponentFactory(settings);
            _paths = new();
        }

        /// <summary>
        /// ����� �������������
        /// </summary>
        public void Initialize()
        {
            AddPath();
        }

        /// <summary>
        /// ����� ���������� ����� ������
        /// </summary>
        public void AddPath()
        {
            var path = _factory.Create();
            var visualizer = new PathPointersVisualizer(path);
            _paths.Add(path, visualizer);
            path.SetMaterial(_settings.Material);

            if (_isVisiblePointers)
                visualizer.Show();
            else 
                visualizer.Hide();

            _selectedPath = path;
        }

        /// <summary>
        /// ����� ������������ ��������� ���������� ������
        /// </summary>
        public void SwitchVisiblePointers()
        {
            _isVisiblePointers = !_isVisiblePointers;

            if (_isVisiblePointers)
                ShowAllPaths();
            else
                HideAllPaths();
        }

        /// <summary>
        /// ����� ����������� ���� ���������� ������
        /// </summary>
        public void ShowAllPaths()
        {
            foreach (var valuePair in _paths)
            {
                valuePair.Value.Show();
            }
        }

        /// <summary>
        /// ����� ������� ���� ���������� ������
        /// </summary>
        public void HideAllPaths()
        {
            foreach (var valuePair in _paths)
            {
                valuePair.Value.Hide();
            }
        }

        /// <summary>
        /// ����� ����������� ���������� ���������� ������
        /// </summary>
        /// /// <param name="path">������ ������</param>
        public void ShowPath(PathComponent path)
        {
            if (_paths.TryGetValue(path, out var visualizer))
            {
                visualizer.Show();
            }
        }

        /// <summary>
        /// ����� ������� ���������� ���������� ������
        /// </summary>
        /// /// <param name="path">������ ������</param>
        public void HidePaths(PathComponent path)
        {
            if (_paths.TryGetValue(path, out var visualizer))
            {
                visualizer.Hide();
            }
        }

        /// <summary>
        /// ����� ������ ��������� ������
        /// </summary>
        /// <param name="pointer">��������� ������</param>
        public void SelectPointer(PathPointer pointer)
        {
            pointer.SetSelected(true);
            _selectedPointer = pointer;
        }

        /// <summary>
        /// ����� �������� ��������� ������
        /// </summary>
        /// <param name="pointer">��������� ������</param>
        public void RemovePointer(PathPointer pointer)
        {
            pointer.Point.MustDestroy = true;
        }

        /// <summary>
        /// ����� ������ ������ ��������� ������
        /// </summary>
        public void UnselectPointer()
        {
            if (_selectedPointer == null) return;
            _selectedPointer.SetSelected(false);
            _selectedPointer = null;
        }

        /// <summary>
        /// ����� ����������� ���������� ��������� ������
        /// </summary>
        /// <param name="position">������� �����������</param>
        public void MoveSelectedPointer(Vector3 position)
        {
            if (_selectedPointer == null) return;
            _selectedPointer.Point.Position = position;
        }

        /// <summary>
        /// ����� ������ ���� �����
        /// </summary>
        public void Reset()
        {
            foreach (var value in _paths.Values)
            {
                value.Dispose();
            }

            foreach (var value in _paths.Keys)
            {
                value.Destroy();
            }

            _paths.Clear();
            AddPath();
        }

        /// <summary>
        /// ����� ���������� ������ ��������� ������
        /// </summary>
        /// <param name="position">������� ���������</param>
        public void AddPointer(Vector3 position)
        {
            _selectedPath.AddPoint(position);
        }
    }
}

﻿using PathGeneration.Enviroments;
using PathGeneration.Input;
using PathGeneration.Interactions;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PathGeneration.Core
{
    /// <summary>
    /// Класс управления расстановщиком дорог  
    /// </summary>
    public class PathMapperController : InteractionInput.IBaseActions
    {
        /// <summary>
        /// Поле расстановщика дорог  
        /// </summary>
        private readonly PathMapper _mapper;

        /// <summary>
        /// Поле утилиты raycast
        /// </summary>
        private readonly RaycastUtility _raycaster;

        /// <summary>
        /// Поле флаг алтернативного действия
        /// </summary>
        private bool _isAltAction;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="pathMapper">расстановщик дорог</param>
        /// <param name="raycaster"> утилита raycast</param>
        /// <param name="input">управление взаимодействием</param>
        public PathMapperController(PathMapper pathMapper, RaycastUtility raycaster, InteractionInput input)
        {
            _mapper = pathMapper;
            _raycaster = raycaster;
            input.Base.SetCallbacks(this);
            input.Enable();
        }

        /// <summary>
        /// Метод очистки дорог
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnClear(InputAction.CallbackContext context)
        {
            if (context.started)
                _mapper.Reset();
        }

        /// <summary>
        /// Метод скрытия/отображения указателей дороги
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnHide(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                _mapper.SwitchVisiblePointers();
            }
        }

        /// <summary>
        /// Метод взаимодействия с объектами
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnInteract(InputAction.CallbackContext context)
        {
            var position = context.ReadValue<Vector2>();
            var hit = _raycaster.ScreenPointToHit(position);
            if (hit.HasValue && TryGetInteractableFromHit(hit.Value, out var interactable))
            {
                var point = hit.Value.point;
                switch (context.phase)
                {
                    case InputActionPhase.Started:
                        OnStartedInteraction(interactable, point);
                        break;
                    case InputActionPhase.Performed:
                        OnPerformedInteraction(interactable, point);
                        break;
                    case InputActionPhase.Canceled:
                        OnCanceledInteraction(interactable, point);
                        break;
                }
            }
        }

        /// <summary>
        /// Метод переключения альтернативного действия
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnAltAction(InputAction.CallbackContext context)
        {
            if (context.started)
                _isAltAction = true;
            else if (context.canceled)
                _isAltAction = false;
        }

        /// <summary>
        /// Метод создания новой дороги
        /// </summary>
        /// <param name="context">Контекст управления</param>
        public void OnCreate(InputAction.CallbackContext context)
        {
            if (context.started)
                _mapper.AddPath();
        }

        /// <summary>
        /// Метод начала взаимодействия с объектами
        /// </summary>
        /// <param name="interactable">объект взаимодействия</param>
        /// <param name="point">позиция взаимодействия</param>
        private void OnStartedInteraction(IInteractable interactable, Vector3 point)
        {
            if (interactable is Ground ground)
                OnGroundInteraction(ground, point);
            else if (interactable is PathPointer pointer)
                OnPathPointerInteraction(pointer, point);
        }

        /// <summary>
        /// Метод обработчик выполнения взаимодействия
        /// </summary>
        /// <param name="interactable">объект взаимодействия</param>
        /// <param name="point">позиция взаимодействия</param>
        private void OnPerformedInteraction(IInteractable interactable, Vector3 point)
        {
            _mapper.MoveSelectedPointer(point);
        }

        /// <summary>
        /// Метод обработчик завершения взаимодействия
        /// </summary>
        /// <param name="interactable">объект взаимодействия</param>
        /// <param name="point">позиция взаимодействия</param>
        private void OnCanceledInteraction(IInteractable interactable, Vector3 point)
        {
            _mapper.UnselectPointer();
        }

        /// <summary>
        /// Метод обработчик выполнения взаимодействия с поверхностью
        /// </summary>
        /// <param name="ground">взаимодействующая поверхность</param>
        /// <param name="point">позиция взаимодействия</param>
        private void OnGroundInteraction(Ground ground, Vector3 point)
        {
            _mapper.AddPointer(point);
        }

        /// <summary>
        /// Метод обработчик выполнения взаимодействия с указателем
        /// </summary>
        /// <param name="pointer">взаимодействующий указатель</param>
        /// <param name="point">позиция взаимодействия</param>
        private void OnPathPointerInteraction(PathPointer pointer, Vector3 point)
        {
            if (_isAltAction)
                _mapper.RemovePointer(pointer);
            else
                _mapper.SelectPointer(pointer);
        }

        /// <summary>
        /// Метод попытки получения взаимодействующего объекта
        /// </summary>
        /// <param name="hit">информация о соприкосновении с объектом</param>
        /// <param name="interactable"> результат соприкосновения объекта</param>
        /// <returns>если true - соприкосновении с объектом, false - нет соприкосновении с объектом</returns>
        private bool TryGetInteractableFromHit(RaycastHit hit, out IInteractable interactable)
        {
            interactable = hit.collider.GetComponent<IInteractable>();
            if (interactable == null)
                return false;
            return true;
        }
    }
}

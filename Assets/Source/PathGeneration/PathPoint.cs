﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace PathGeneration.Core
{
    /// <summary>
    /// Класс точки поворотов дороги
    /// </summary>
    public class PathPoint : INotifyPropertyChanged
    {
        /// <summary>
        /// поле позиции точки
        /// </summary>
        private Vector3 _position;

        /// <summary>
        /// поле флаг о необходимости уничтожить дорогу
        /// </summary>
        private bool _mustDestroy;

        /// <summary>
        /// Свойство позиции точки
        /// </summary>
        public Vector3 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Свойство флага о необходимости уничтожить дорогу
        /// </summary>
        public bool MustDestroy
        {
            get { return _mustDestroy; }
            set
            {
                _mustDestroy = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="position">позиция точки</param>
        public PathPoint(Vector3 position)
        {
            Position = position;
        }
        
        /// <summary>
        /// Делегат изменения свойств класса
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Метод обработчик изменения свойств класса
        /// </summary>
        /// <param name="prop">название свойства</param>
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using UnityEngine;

namespace PathGeneration.Core
{
    /// <summary>
    /// ����� ������� �������� ���� ������
    /// </summary>
    public class PathMeshFactory
    {
        /// <summary>
        /// ���� ������� �������� ������
        /// </summary>
        private float _curveRadius;

        /// <summary>
        /// ���� ������ ������
        /// </summary>
        private float _width;

        /// <summary>
        /// ���� ���������� �������� � �������� ������
        /// </summary>
        private int _sectors;

        /// <summary>
        /// ����� �������� ���� ������
        /// </summary>
        /// <param name="points">������ ����� ������</param>
        /// <param name="width">������ ������, �� ��������� 1</param>
        /// <param name="curveRadius">������ �������� ������, �� ��������� 1</param>
        /// <param name="sectors">���������� �������� � �������� ������, �� ��������� 10</param>
        /// <returns>��� ������</returns>
        public Mesh Create(IEnumerable<Vector3> points, float width = 1, float curveRadius = 1, int sectors = 10)
        {
            _width = width;
            _curveRadius = curveRadius;
            _sectors = sectors;
            return CalculatePathMesh(points.ToList());
        }

        /// <summary>
        /// ����� ������� ���� ������
        /// </summary>
        /// <param name="points">������ ����� ������</param>
        /// <returns>��� ������</returns>
        protected Mesh CalculatePathMesh(List<Vector3> points)
        {
            var lines = CalculateLines(points);
            var turns = CalculateTurns(lines);
            var vertices = CalculateVertices(lines, turns);
            var triangles = CalculateTriangles(vertices.Count);

            return CreateMesh(vertices, triangles);
        }

        /// <summary>
        /// ����� ������� �������� ������ ������
        /// </summary>
        /// <param name="points">������ ����� ������</param>
        /// <returns>������ �������� ������ ������</returns>
        private List<PathLine> CalculateLines(List<Vector3> points)
        {
            var lines = new List<PathLine>();
            for (int i = 0; i < points.Count - 1; i++)
            {
                var startPoint = points[i];
                var endPoint = points[i + 1];
                var direction = (endPoint - startPoint).normalized;
                var normal = Quaternion.AngleAxis(90, Vector3.up) * direction;
                var line = new PathLine(startPoint, endPoint, direction, normal);
                lines.Add(line);
            }

            return lines;
        }

        /// <summary>
        /// ����� ������� ��������� ������
        /// </summary>
        /// <param name="lines">������ �������� ������ ������</param>
        /// <returns>������ ����� �������� ������</returns>
        private List<float> CalculateTurns(List<PathLine> lines)
        {
            var turns = new List<float>();
            for (int i = 0; i < lines.Count - 1; i++)
            {
                var angle = Vector3.SignedAngle(lines[i].Direction, lines[i + 1].Direction, Vector3.up);
                turns.Add(angle);
            }

            return turns;
        }

        /// <summary>
        /// ����� ������� ������ ���� ������
        /// </summary>
        /// <param name="lines">>������ �������� ������ ������</param>
        /// <param name="turns">������ ����� �������� ������</param>
        /// <returns>������ ������ ���� ������</returns>
        private List<Vector3> CalculateVertices(List<PathLine> lines, List<float> turns)
        {
            var vertices = new List<Vector3>();
            for (int i = 0; i < lines.Count; i++)
            {
                var startAngle = i == 0 ? 0 : turns[i - 1];
                var endAngle = i == lines.Count - 1 ? 0 : turns[i];
                var origin = CreateCurvedLine(vertices, lines[i], startAngle, endAngle);
                if (endAngle != 0)
                {
                    var points = CalculateCurvePoints(endAngle, lines[i].Direction);
                    CreateCurve(ref vertices, points, origin, endAngle > 0);
                }
            }

            return vertices;
        }

        /// <summary>
        /// ����� �������� �������� ������ � ���������
        /// </summary>
        /// <param name="vertices">������ ������ ���� ������</param>
        /// <param name="line">������ �������� ������ ������</param>
        /// <param name="startAngle">���� �������� � ������ �������</param>
        /// <param name="endAngle">���� �������� � ����� �������</param>
        /// <returns>������� ������ ���������� �������� � ����� �������</returns>
        private Vector3 CreateCurvedLine(List<Vector3> vertices, PathLine line, float startAngle, float endAngle)
        {
            var startNormal = RotatePathNormal(line.Normal, -startAngle * 0.5f);
            var endNormal = RotatePathNormal(line.Normal, endAngle * 0.5f);
            var startInverse = startAngle < 0;
            var endInverse = endAngle < 0;

            var linePoint0 = line.StartPoint + startNormal * _width * 0.5f;
            var linePoint1 = line.StartPoint - startNormal * _width * 0.5f;
            var linePoint2 = line.EndPoint + endNormal * _width * 0.5f;
            var linePoint3 = line.EndPoint - endNormal * _width * 0.5f;

            var startInTan = CalculateCurveTangent(Mathf.Abs(startAngle), _curveRadius);
            var endInTan = CalculateCurveTangent(Mathf.Abs(endAngle), _curveRadius);
            var startOutTan = CalculateCurveTangent(Mathf.Abs(startAngle), _curveRadius + _width);
            var endOutTan = CalculateCurveTangent(Mathf.Abs(endAngle), _curveRadius + _width);


            var curveStartPoint0 = linePoint0 + (!startInverse ? startInTan : startOutTan) * line.Direction;
            var curveStartPoint1 = linePoint1 + (startInverse ? startInTan : startOutTan) * line.Direction;
            var curveStartPoint2 = linePoint2 - (!endInverse ? endInTan : endOutTan) * line.Direction;
            var curveStartPoint3 = linePoint3 - (endInverse ? endInTan : endOutTan) * line.Direction;

            vertices.Add(curveStartPoint0);
            vertices.Add(curveStartPoint1);
            vertices.Add(curveStartPoint2);
            vertices.Add(curveStartPoint3);

            var origin = (!endInverse ? curveStartPoint2 : curveStartPoint3) + Mathf.Sign(endAngle) * (curveStartPoint2 - curveStartPoint3).normalized * _curveRadius;
            return origin;
        }

        /// <summary>
        /// ����� �������� ������� ������� ������
        /// </summary>
        /// <param name="normal">������� ������� ������</param>
        /// <param name="angle">���� ��������</param>
        /// <returns>���������� ������� ������� ������</returns>
        private Vector3 RotatePathNormal(Vector3 normal, float angle)
        {
            return Quaternion.AngleAxis(angle, Vector3.up) * normal / Mathf.Cos(Mathf.Deg2Rad * angle);
        }

        /// <summary>
        /// ����� ������� ��������� ��������
        /// </summary>
        /// <param name="angle">���� ��������</param>
        /// <param name="radius">������ ��������</param>
        /// <returns>������� ��������</returns>
        private float CalculateCurveTangent(float angle, float radius)
        {
            return radius * Mathf.Tan(Mathf.Deg2Rad * angle / 2);
        }

        /// <summary>
        /// ����� ������� ������� ������ � ��������
        /// </summary>
        /// <param name="turnAngle">���� ��������</param>
        /// <param name="direction">����������� ������� ������</param>
        /// <returns>������ ������� ������ � ��������</returns>
        private List<Vector3> CalculateCurvePoints(float turnAngle, Vector3 direction)
        {
            var points = new List<Vector3>();
            var turnRad = turnAngle * Mathf.Deg2Rad;
            var offsetRad = (turnAngle < 0 ? Mathf.PI : 0) + Vector3.SignedAngle(Vector3.right, direction, Vector3.up) * Mathf.Deg2Rad;

            for (int i = 1; i < _sectors; i++)
            {
                float rad = offsetRad + turnRad * i / _sectors;
                points.Add(new Vector3(Mathf.Sin(rad), 0, Mathf.Cos(rad)));
            }
            return points;
        }

        /// <summary>
        /// ����� �������� �������� ������
        /// </summary>
        /// <param name="vertices">������ ������ ����</param>
        /// <param name="points">������ ������� ������ � ��������</param>
        /// <param name="origin">������� ������ ���������� ��������</param>
        /// <param name="isRight">���� ������� ��������, ���� true ������� ��������� �� �����, false - �� ����</param>
        private void CreateCurve(ref List<Vector3> vertices, List<Vector3> points, Vector3 origin, bool isRight)
        {
            foreach (var point in points)
            {
                var inVertex = origin + point * _curveRadius;
                var outVertex = origin + point * (_curveRadius + _width);

                vertices.Add(isRight ? inVertex : outVertex);
                vertices.Add(isRight ? outVertex : inVertex);
            }
        }

        /// <summary>
        /// ����� ������� ������������� ����
        /// </summary>
        /// <param name="vectixCount">���������� ����� ����</param>
        /// <returns>������ ������������� ����</returns>
        private List<int> CalculateTriangles(int vectixCount)
        {
            var triangles = new List<int>();
            for (int i = 0; i < vectixCount / 2 - 1; i++)
            {
                triangles.Add(2 * i);
                triangles.Add(2 * i + 1);
                triangles.Add(2 * i + 2);

                triangles.Add(2 * i + 1);
                triangles.Add(2 * i + 3);
                triangles.Add(2 * i + 2);
            }

            return triangles;
        }

        /// <summary>
        /// ����� �������� ����
        /// </summary>
        /// <param name="vertices">������ ���� ������ ����</param>
        /// <param name="triangles">������ ���� ������������� ����</param>
        /// <returns>��� ������</returns>
        private Mesh CreateMesh(List<Vector3> vertices, List<int> triangles)
        {
            var mesh = new Mesh();
            mesh.SetVertices(vertices);
            mesh.SetTriangles(triangles, 0);
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            mesh.name = "Path";
            return mesh;
        }
    }
}

﻿using UnityEngine;

namespace PathGeneration.Core
{
    /// <summary>
    /// Структура участка дороги
    /// </summary>
    public struct PathLine
    {
        /// <summary>
        /// Поле начальной точки участка дороги
        /// </summary>
        public readonly Vector3 StartPoint;

        /// <summary>
        /// Поле конечной точки участка дороги
        /// </summary>
        public readonly Vector3 EndPoint;

        /// <summary>
        /// Поле направления участка дороги
        /// </summary>
        public readonly Vector3 Direction;

        /// <summary>
        /// Поле нормали участка дороги
        /// </summary>
        public readonly Vector3 Normal;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="startPoint">начальная точка участка дороги</param>
        /// <param name="endPoint"> конечная точка участка дороги</param>
        /// <param name="direction">направление участка дороги</param>
        /// <param name="normal">нормаль участка дороги</param>
        public PathLine(Vector3 startPoint, Vector3 endPoint, Vector3 direction, Vector3 normal)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
            Direction = direction;
            Normal = normal;
        }
    }
}

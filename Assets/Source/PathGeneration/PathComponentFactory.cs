﻿using PathGeneration.Enviroments;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace PathGeneration.Core
{
    /// <summary>
    /// Класс фабрики объекта дороги
    /// </summary>
    public class PathComponentFactory
    {
        /// <summary>
        /// Поле настроек дороги
        /// </summary>
        private readonly PathSettings _settings;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="settings">настройки дороги</param>
        public PathComponentFactory(PathSettings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Метод создания объекта дороги
        /// </summary>
        /// <returns></returns>
        public PathComponent Create()
        {
            var path = new GameObject("Path").AddComponent<PathComponent>();
            path.SetPathSettings(_settings);
            return path;
        }
    }
}

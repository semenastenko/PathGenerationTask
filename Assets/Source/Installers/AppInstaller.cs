using PathGeneration.Cameras;
using PathGeneration.Core;
using PathGeneration.Enviroments;
using PathGeneration.Input;
using Zenject;

namespace PathGeneration.Installers
{
    /// <summary>
    /// Zenject ����������, ������� ��������� ��� ����������� � �� ��������� ���� � ������.
    /// </summary>
    public class AppInstaller : MonoInstaller
    {
        /// <summary>
        /// ����� ��� ���������� ������������.
        /// </summary>
        public override void InstallBindings()
        {
            InstallInputBindings();
            InstallPathMapper();
            InstallCameraBehavioursr();
        }

        /// <summary>
        /// ����� ��� ���������� ������������ ����������.
        /// </summary>
        private void InstallInputBindings()
        {
            Container.BindInterfacesAndSelfTo<InteractionInput>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<CameraInput>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<CameraStateInput>().AsSingle().NonLazy();
        }

        /// <summary>
        /// ����� ��� ���������� ������������ ���������� �����.
        /// </summary>
        private void InstallPathMapper()
        {
            Container.BindInterfacesAndSelfTo<PathMapper>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<RaycastUtility>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<PathMapperController>().AsSingle().NonLazy();
        }

        /// <summary>
        /// ����� ��� ���������� ������������ ��������� ������.
        /// </summary>
        private void InstallCameraBehavioursr()
        {
            Container.BindInterfacesAndSelfTo<CameraBehaviourManager>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<CameraBehaviourSwitcher>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<FlightCameraBehaviour>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<TopViewCameraBehaviour>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<DefaultBehaviourHolder<FlightCameraBehaviour>>().AsSingle().NonLazy();
        }
    }
}
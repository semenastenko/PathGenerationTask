using PathGeneration.Enviroments;
using UnityEngine;
using Zenject;

namespace PathGeneration.Installers
{
    /// <summary>
    /// Zenject ���������� ��� ScriptableObject, ������� ��������� ��� ����������� ScriptableObject � �� ��������� ���� � ������.
    /// </summary>
    [CreateAssetMenu(fileName = "SOInstaller", menuName = "Installers/SOInstaller")]
    public class SOInstaller : ScriptableObjectInstaller<SOInstaller>
    {
        /// <summary>
        /// ���� �������� ������.
        /// </summary>
        [SerializeField]
        private CameraSettings _cameraSettings;

        /// <summary>
        /// ���� �������� ������.
        /// </summary>
        [SerializeField]
        private PathSettings _pathSettings;

        /// <summary>
        /// ����� ��� ���������� ������������ ScriptableObject.
        /// </summary>
        public override void InstallBindings()
        {
            Container.BindInstance(_cameraSettings);
            Container.BindInstance(_pathSettings);
        }
    }
}